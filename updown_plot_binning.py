#!/usr/bin/env python3

import uproot
import json
import numpy as np
from pprint import pprint
import matplotlib as mpl
import matplotlib.pyplot as plt
import awkward as ak
import hist
import mplhep as hep
import boost_histogram as bh
import sys
import os
import argparse
import config_parser_binning
import gc
import copy

plt.style.use(hep.style.ATLAS)

def get_counts_with_overflow_added(h):
#     counts_temp = h.counts(flow=True)
#     counts = copy.deepcopy(counts_temp[:-1])
#     counts[-1] += counts_temp[-1]
#     print(counts_temp)
#     print(counts)
    return h.counts(flow=True)

def get_scale_variation_hists(scale_variations,x_array,x_bins,weights,names,LHE3Weights_array,len_info,h_nom,cuts,groupstr):
        #x_array = awk array of all "x" which can be any event attribute
        #h_nom = nominal hist histogram
        #weights = genWeights
        #names = LHE3Weight names list
        #batch_size = len(names)
        #LHE3Weights_array = LHE3Weights
        #cuts = ('cuts_name','cutstring')
        if groupstr == 'envelope':
                #get envelope (array of (env up,env down)) for all bins
                nom_counts = get_counts_with_overflow_added(h_nom)
                envmax = np.zeros(len(nom_counts))
                envmin = np.ones(len(nom_counts))*np.inf
                for gstr in ['MUR','MUF','MURMUF']:
                        h_up,h_down = get_scale_variation_hists(scale_variations,x_array,x_bins,weights,names,LHE3Weights_array,len_info,h_nom,cuts,gstr)
                        up_counts,down_counts = get_counts_with_overflow_added(h_up),get_counts_with_overflow_added(h_down)
                        maxcounts = np.max(np.column_stack((up_counts,down_counts)),axis=1)
                        mincounts = np.min(np.column_stack((up_counts,down_counts)),axis=1)
                        max_indices = np.where(maxcounts > envmax)
                        envmax[max_indices] = maxcounts[max_indices]
                        min_indices = np.where(mincounts < envmin)
                        envmin[min_indices] = mincounts[min_indices]
                left_bins = h_nom.axes.edges[0]
                h_up = hist.Hist.new.Var(x_bins[:-1],name='up',overflow=True,underflow=False).Weight()
                h_up.fill(left_bins,weight=envmax)
                h_down = hist.Hist.new.Var(x_bins[:-1],name='down',overflow=True,underflow=False).Weight()
                h_down.fill(left_bins,weight=envmin)
                return h_up,h_down


        up_weight = scale_variations[groupstr+'up']
        down_weight = scale_variations[groupstr+'down']
        Ntrees = len(len_info['W'])
        
        #up/down variations
        h_up = hist.Hist.new.Var(x_bins[:-1],name='up',overflow=True,underflow=False).Weight()
        h_up_weights = []
        h_down = hist.Hist.new.Var(x_bins[:-1],name='down',overflow=True,underflow=False).Weight()
        h_down_weights = [] 
        up_weightindex = list(names).index(up_weight)
        down_weightindex = list(names).index(down_weight)
        Nnames = len_info['LWN'][0]
        for b in range(Ntrees): #for each tree, append weights
                #print("up_weightindex =",up_weightindex)
                #print(LHE3Weights_array.LHE3Weights)
                #print(len(LHE3Weights_array.LHE3Weights))
                #print(len(LHE3Weights_array.LHE3Weights[0]))
                #print("batch size =",batch_size)
                Ngenweights = len_info['W'][b]
                genweight_startindex = sum(len_info['W'][:b])
                n_empty = 0
                for i in range(genweight_startindex,genweight_startindex + Ngenweights):
                    if len(LHE3Weights_array[i])==0:
                        n_empty += 1
                        h_up_weights.append(0.)
                        h_down_weights.append(0.)
                        continue
                    h_up_weights.append(weights[i] * LHE3Weights_array[i][up_weightindex])
                    h_down_weights.append(weights[i] * LHE3Weights_array[i][down_weightindex])
                if n_empty > 0:
                    print(f"WARNING empty LHE3Weights_array set to zero: {n_empty}")
        h_up.fill(x_array,weight=h_up_weights)
        h_down.fill(x_array,weight=h_down_weights)

        return h_up,h_down

def get_pdf_variation_hists(pdf_variations,x_array,x_bins,weights,names,LHE3Weights_array,len_info,cuts):
        weightnames = [x[1] for x in pdf_variations]
        Ntrees = len(len_info['W'])
        h_weights_list = []
        for weightname in weightnames:
                print(weightname)
                h_weights = []
                weightindex = list(names).index(weightname)
                for b in range(Ntrees):
                        Ngenweights = len_info['W'][b]
                        genweight_startindex = sum(len_info['W'][:b])
                        n_empty = 0
                        for i in range(genweight_startindex,genweight_startindex+Ngenweights):
                                if len(LHE3Weights_array[i])==0:
                                        n_empty += 1
                                        h_weights.append(0.)
                                        continue
                                h_weights.append(weights[i]*LHE3Weights_array[i][weightindex])
                        if n_empty > 0:
                                print(f"WARNING empty LHE3Weights_array set to zero: {n_empty}")
                h_weights_list.append(h_weights)
        weights_by_bin = np.transpose(np.array(h_weights_list))
        #average weights for a single bin across all variations
        avg_bin_weights = np.array([np.average(bin_weights) for bin_weights in weights_by_bin])
        std_bin_weights = np.array([np.std(bin_weights) for bin_weights in weights_by_bin])
        
        h_up = hist.Hist.new.Var(x_bins[:-1],name='upvar',overflow=True,underflow=False).Weight()
        avg_up = avg_bin_weights + std_bin_weights
        h_up.fill(x_array,weight=avg_up)

        h_down = hist.Hist.new.Var(x_bins[:-1],name='downvar', overflow=True,underflow=False).Weight()
        avg_down = avg_bin_weights - std_bin_weights
        h_down.fill(x_array,weight=avg_down)

        return h_up,h_down

def get_ratios(counts,nominal_counts):
        ratios = np.ones(len(counts))
        for i in range(len(counts)):
                if nominal_counts[i] == 0:
                        ratios[i] = 1 #not sure what else to do here
                else:
                        ratios[i] = counts[i] / nominal_counts[i]
        return ratios

def plot_scale_variation(x_name,h_nom,h_up,h_down,groupstr,cuts_name,savename,savedir,x_bins):
        linestyle = '--'
        bin_edges = np.array(x_bins)
        nom_counts = get_counts_with_overflow_added(h_nom)
        up_counts,down_counts = get_counts_with_overflow_added(h_up),get_counts_with_overflow_added(h_down)
        
        fig,axs=plt.subplots(2,sharex=True)
        axs[0].set_yscale('log')
        hep.histplot(h_nom,ax=axs[0],linewidth=1,linestyle=linestyle,label="nominal")
        hep.histplot(h_up,ax=axs[0],linewidth=1,label="up")
        hep.histplot(h_down,ax=axs[0],linewidth=1,label="down")
        axs[0].bar(x=bin_edges[:-1], height=up_counts - down_counts, 
                        bottom=down_counts, width=np.diff(bin_edges), 
                        align='edge', linewidth=0, color='red', alpha=0.25, 
                        zorder=-1, label='systematics envelope')
        axs[0].set_title('Up and down {0}: {1}'.format(groupstr,cuts_name))
        axs[0].legend(loc='upper right',prop={'size':6})
        axs[0].set_ylabel('arb.')

        up_ratios,down_ratios = get_ratios(up_counts,nom_counts),get_ratios(down_counts,nom_counts)
        axs[1].hist(bin_edges[:-1],bins=bin_edges,histtype='step',linestyle=linestyle,weights=np.ones_like(nom_counts),label='nominal')
        axs[1].hist(bin_edges[:-1],bins=bin_edges,histtype='step',weights=up_ratios,label='up')
        axs[1].hist(bin_edges[:-1],bins=bin_edges,histtype='step',weights=down_ratios,label='down')
        axs[1].bar(x=bin_edges[:-1], height=up_ratios - down_ratios, 
                        bottom=down_ratios, width=np.diff(bin_edges), 
                        align='edge', linewidth=0, color='red', alpha=0.25, 
                        zorder=-1, label='systematics envelope')

        axs[1].set_xlabel(x_name)
        axs[1].set_ylabel('Ratio to nominal')
        plt.tight_layout()
        plt.savefig(os.path.join(savedir,"{0}_{1}.pdf".format(savename,groupstr)),dpi=70)
        plt.clf()


def plot_all_scale_variations(x_name,h_nom,Nevents,all_hists,cuts_name,savename,savedir,x_bins):
        linestyle = '-'
        bin_edges = np.array(x_bins)
        nom_counts = get_counts_with_overflow_added(h_nom)
        print("nom counts = ",nom_counts)
        fig,ax=plt.subplots(1)
        ax.hist(bin_edges[:-1],bins=bin_edges,histtype='step',weights=np.ones_like(nom_counts),color='black')
        color_cycle = plt.rcParams['axes.prop_cycle'].by_key()['color']
        i_c = 0
        for gstr,h_up,h_down in all_hists:
                up_counts,down_counts = get_counts_with_overflow_added(h_up),get_counts_with_overflow_added(h_down)
                up_ratios,down_ratios = get_ratios(up_counts,nom_counts),get_ratios(down_counts,nom_counts)
                up_err,down_err = h_up.variances(flow=True)**0.5,h_down.variances(flow=True)**0.5
                if gstr == 'envelope':
                        print('up ratios',up_ratios)
                        print('down ratios',down_ratios)
                        print('up_ratios - down_ratios =',up_ratios-down_ratios)
                        ax.bar(x=bin_edges[:-1], height=up_ratios - down_ratios, 
                                bottom=down_ratios, width=np.diff(bin_edges), 
                                align='edge', linewidth=0, color='red', alpha=0.25, 
                                zorder=-1, label='systematics envelope',)
                        ax.set_ylim(bottom=0.9*min(list(up_ratios)+list(down_ratios)))
                        
                        # dump ratio values to json
                        d_scale_ratio = {"up":up_ratios.tolist(), "down":down_ratios.tolist()}
                        with open(f"{savedir}/{savename}_scale_ratio.json","w") as outfile:
                            json.dump(d_scale_ratio,outfile)
                        
                        # write npz histogram files with yields for cabinetry
                        binning_temp = x_bins[:-1]
                        samp_name = savename.split(cuts_name)[0][:-1]
                        syst_name = f"{samp_name}_scale"
                        outfile_up = f'{savedir}/{cuts_name}_{samp_name}_{syst_name}_Up_modified.npz'
                        outfile_down = f'{savedir}/{cuts_name}_{samp_name}_{syst_name}_Down_modified.npz'

                        of_up_counts = get_counts_with_overflow_added(h_up)
                        of_down_counts = get_counts_with_overflow_added(h_down)
                        np.savez(outfile_up,yields=of_up_counts,stdev=up_err,bins=binning_temp)
                        np.savez(outfile_down,yields=of_down_counts,stdev=down_err,bins=binning_temp)

                else:
                        n_up,bin_edges_up,patches_up = ax.hist(
                            bin_edges[:-1],bins=bin_edges,histtype='step',weights=up_ratios,
                            linewidth=2,linestyle=linestyle,label='{0}2'.format(gstr),color=color_cycle[i_c])
                        mid_up = 0.5*(bin_edges_up[1:] + bin_edges_up[:-1])
                        ax.errorbar(mid_up,n_up,yerr=np.nan_to_num(up_err/nom_counts),ls='none',ecolor=color_cycle[i_c])
                        i_c += 1
                        n_down,bin_edges_down,patches_down = ax.hist(
                            bin_edges[:-1],bins=bin_edges,histtype='step',weights=down_ratios,
                            linewidth=2,linestyle=linestyle,label='{0}0.5'.format(gstr),color=color_cycle[i_c])
                        mid_down = 0.5*(bin_edges_down[1:] + bin_edges_down[:-1])
                        ax.errorbar(mid_down,n_down,yerr=np.nan_to_num(down_err/nom_counts),ls='none',ecolor=color_cycle[i_c])
                        i_c += 1

        ax.set_xlabel(x_name,fontsize=14)
        ax.set_ylabel('Ratio to nominal',fontsize=14)
        ax.legend(loc='upper left',prop={'size':6})
        sample_name = savename.split(f"_{cuts_name}")[0]
        ax.set_title('Scale variations for {} in {}'.format(sample_name,cuts_name),fontsize=14)
        ytop,ybottom = ax.get_ylim()
#       plt.text(bin_edges[-2],ytop - (ytop-ybottom)/10,
#                   "Nevents = {0}".format(str(Nevents)),
#                   fontsize=12,ha='center',va='top')
        plt.tight_layout()
        plt.savefig(os.path.join(savedir,"{0}_all.pdf".format(savename)),dpi=70)
        plt.clf()

def plot_pdf_variation(x_name,h_nom,h_up,h_down,cuts_name,savename,savedir,x_bins):
        linestyle = '--'
        bin_edges = np.array(x_bins)
        nom_counts = get_counts_with_overflow_added(h_nom)
        up_counts,down_counts = get_counts_with_overflow_added(h_up),get_counts_with_overflow_added(h_down)
        up_err,down_err = h_up.variances(flow=True)**0.5,h_down.variances(flow=True)**0.5
        
        fig,axs=plt.subplots(2,sharex=True)
        axs[0].set_yscale('log')
        hep.histplot(h_nom,ax=axs[0],linewidth=1,linestyle=linestyle,label="nominal")
        hep.histplot(h_up,ax=axs[0],linewidth=1,label="up")
        hep.histplot(h_down,ax=axs[0],linewidth=1,label="down")
        axs[0].bar(x=bin_edges[:-1], height=up_counts - down_counts, 
                        bottom=down_counts, width=np.diff(bin_edges), 
                        align='edge', linewidth=0, color='red', alpha=0.25, 
                        zorder=-1, label='systematics envelope')
        sample_name = savename.split(f"_{cuts_name}")[0]
        axs[0].set_title('PDF variations for {} in {}'.format(sample_name,cuts_name),fontsize=14)
        axs[0].legend(loc='upper right',prop={'size':6})
        axs[0].set_ylabel('arb.',fontsize=14)

        up_ratios,down_ratios = get_ratios(up_counts,nom_counts),get_ratios(down_counts,nom_counts)
        axs[1].hist(bin_edges[:-1],bins=bin_edges,histtype='step',weights=np.ones_like(nom_counts),linestyle='--')
        axs[1].hist(bin_edges[:-1],bins=bin_edges,histtype='step',weights=up_ratios,label='up')
        axs[1].hist(bin_edges[:-1],bins=bin_edges,histtype='step',weights=down_ratios,label='down')
        axs[1].bar(x=bin_edges[:-1], height=up_ratios - down_ratios, 
                        bottom=down_ratios, width=np.diff(bin_edges), 
                        align='edge', linewidth=0, color='red', alpha=0.25, 
                        zorder=-1, label='systematics envelope')
        axs[1].set_xlabel(x_name,fontsize=14)
        axs[1].set_ylabel('Ratio to nominal',fontsize=14)
        plt.tight_layout()
        plt.savefig(os.path.join(savedir,'{0}_PDF.pdf'.format(savename)),dpi=70)
        plt.clf()

        # ratio only plot
        fig_r,ax_r=plt.subplots(1)
        ax_r.hist(
            bin_edges[:-1],bins=bin_edges,histtype='step',weights=np.ones_like(nom_counts),linestyle='--')
        n_up,bin_edges_up,patches_up = ax_r.hist(bin_edges[:-1],bins=bin_edges,histtype='step',weights=up_ratios,label='up',color="orange")
        mid_up = 0.5*(bin_edges_up[1:] + bin_edges_up[:-1])
        ax_r.errorbar(mid_up,n_up,yerr=np.nan_to_num(up_err/nom_counts),ecolor="orange",ls='none')
        n_down,bin_edges_down,patches_down = ax_r.hist(bin_edges[:-1],bins=bin_edges,histtype='step',weights=down_ratios,label='down',color="green")
        mid_down = 0.5*(bin_edges_down[1:] + bin_edges_down[:-1])
        ax_r.errorbar(mid_down,n_down,yerr=np.nan_to_num(down_err/nom_counts),ecolor="green",ls='none')
        ax_r.bar(x=bin_edges[:-1], height=up_ratios - down_ratios, 
                        bottom=down_ratios, width=np.diff(bin_edges), 
                        align='edge', linewidth=0, color='red', alpha=0.25, 
                        zorder=-1, label='systematics envelope',)
        ax_r.set_xlabel(x_name,fontsize=14)
        ax_r.set_title('PDF variations for {} in {}'.format(sample_name,cuts_name),fontsize=14)
        ax_r.legend(loc='upper left',prop={'size':6})
        ax_r.set_ylabel('Ratio to nominal',fontsize=14)
        ax_r.set_ylim(bottom=0.9*min(list(up_ratios)+list(down_ratios)))
        plt.tight_layout()
        plt.savefig(os.path.join(savedir,'{0}_PDF_ratio.pdf'.format(savename)),dpi=70)
        plt.clf()
        
        # dump ratio values to json
        d_pdf_ratio = {"up":up_ratios.tolist(), "down":down_ratios.tolist()}
        with open(f"{savedir}/{savename}_PDF_ratio.json","w") as outfile:
            json.dump(d_pdf_ratio,outfile)
            
        # write npz histogram files with yields for cabinetry
        binning_temp = x_bins[:-1]
        samp_name = savename.split(cuts_name)[0][:-1]
        syst_name = f"{samp_name}_pdf"
        outfile_up = f'{savedir}/{cuts_name}_{samp_name}_{syst_name}_Up_modified.npz'
        outfile_down = f'{savedir}/{cuts_name}_{samp_name}_{syst_name}_Down_modified.npz'
        
        of_up_counts = get_counts_with_overflow_added(h_up)
        of_down_counts = get_counts_with_overflow_added(h_down)
        np.savez(outfile_up,yields=of_up_counts,stdev=up_err,bins=binning_temp)
        np.savez(outfile_down,yields=of_down_counts,stdev=down_err,bins=binning_temp)

def digest_tree(tree,x_name,cutstring,entry_stop=None):
        _len_info = {}
#       tree.show()
        l_weights = ["genWeight","eventWeight","leptonWeight","jvtWeight","bTagWeight","pileupWeight","FFWeight"]
        weight_expr = f"{' * '.join(l_weights)} * 139000.0"
        do_iterate = False
        if do_iterate:
#           LHE3Weights_array = ak.Array([])
#           x_array = ak.Array([])
#           weights = ak.Array([])
# #             LHE3Weights_array = []
# #             x_array = []
# #             weights = []
#           step_size = 5000
#           for a in tree.iterate([weight_expr,'LHE3Weights',x_name],cutstring,step_size=step_size,
#               entry_stop=entry_stop):
#               LHE3Weights_array = ak.concatenate([ LHE3Weights_array,a['LHE3Weights'] ],axis=0)
#               x_array = ak.concatenate([ x_array,a[x_name] ],axis=0)
#               weights = ak.concatenate([ weights,a["0"] ],axis=0) # assumes the weights are in the zeroth slot
# #                 LHE3Weights_array.extend( a['LHE3Weights'].to_list() )
# #                 x_array.extend( a[x_name].to_list() )
# #                 weights.extend( a["0"].to_list() ) # assumes the weights are in the zeroth slot
#               del a
#               gc.collect()
            pass
        else:
            LHE3Weights_array = tree.arrays("LHE3Weights",cutstring,entry_stop=entry_stop,library="np")["LHE3Weights"]
            x_array = tree.arrays(x_name,cutstring,entry_stop=entry_stop,library="np")[x_name]
            weights = tree.arrays(weight_expr,cutstring,entry_stop=entry_stop,library="np")[weight_expr]
        _len_info['LW'] = len(LHE3Weights_array)
        _len_info['W'] = len(weights)
        return LHE3Weights_array,x_array,weights,_len_info

def plot_process_with_cuts(paths,cuts,scale_variations,pdf_variations,x_name,x_bins,savename,savedir,entry_stop=None):

        cuts_name,cutstring = cuts
        print("Loading root files...")
        trees = [uproot.open(path) for path in paths]

        #cutflow(trees[0])
        #sys.exit()
        #first digest tree 1, then loop through other trees adding _data to data
        len_info = {}
        LHE3Weights_array,x_array,weights,_len_info = digest_tree(trees[0],x_name,cutstring,entry_stop=entry_stop)
#       print(x_array)
        #assume weightnames are same for all events
        names = trees[0].arrays('LHE3WeightNames', entry_stop=1).LHE3WeightNames[0]
        print(ak.to_list(names))
        _len_info['LWN'] = len(names)
        #consistency check
        print("consistency check...")
        """
        for i in range(_len_info['LW']):
                if i % 1000 == 0: print('...checking event',i)
                _names = trees[0].arrays('LHE3WeightNames', entry_start=i,entry_stop=i+1).LHE3WeightNames[0]
                if len(_names) != len(names):
                        print(len(_names),len(names))
                        raise Exception('Event {0} has wrong number of LHE3WeightNames: {1}. Should be {2}.'.format(i,len(_names),len(names)))
        """
        for k in _len_info:
                len_info[k] = [_len_info[k]]

        for i in range(1,len(trees)):
            _LHE3Weights_array,_x_array,_weights,_len_info = digest_tree(trees[i],x_name,cutstring,entry_stop=entry_stop)
            for k in _len_info:
                    len_info[k].append(_len_info[k])
        
            x_array = ak.concatenate((x_array,_x_array),axis=0)
            weights = ak.concatenate((weights,_weights),axis=0)
            LHE3Weights_array = ak.concatenate((LHE3Weights_array,_LHE3Weights_array),axis=0)
            len_info['LWN'].append(len(names))
            
        #pprint(len_info)
        if len_info['LWN'] != len(trees)*[len(names)]:
                raise Exception("LHE3WeightNames not consistent across trees")

        Nevents = len(weights)
        h_nom = hist.Hist.new.Var(x_bins[:-1],name="nominal",overflow=True,underflow=False).Weight()
        h_nom.fill(x_array,weight=weights)
        #plot scale variations
        all_hists = []
        for groupstr in ['MUR','MUF','MURMUF','envelope']:
                print("Getting scale variation hists for {0}".format(groupstr))
                h_up,h_down = get_scale_variation_hists(scale_variations,x_array,x_bins,weights,names,LHE3Weights_array,len_info,h_nom,cuts,groupstr)
                all_hists.append((groupstr,h_up,h_down))
                plot_scale_variation(x_name,h_nom,h_up,h_down,groupstr,cuts_name,savename,savedir,x_bins)
                print("Plotting scale variation hists for {0}".format(groupstr))

        print("Plotting figure with all scale variations")
        plot_all_scale_variations(x_name,h_nom,Nevents,all_hists,cuts_name,savename,savedir,x_bins)

        #plot averaged pdf variations
        print("Getting pdf variation hists")
        h_up,h_down = get_pdf_variation_hists(pdf_variations,x_array,x_bins,weights,names,LHE3Weights_array,len_info,cuts)
        print("Plotting pdf variation hists")
        plot_pdf_variation(x_name,h_nom,h_up,h_down,cuts_name,savename,savedir,x_bins)

def main(config_file):
        plots_dir = 'plots'
        process_name,hist_info,scale_variations,pdf_variations,cuts_tuple,data_paths = config_parser_binning.parse(config_file)

        cuts_name,cutstring = cuts_tuple
        exiting = False
        for i,(x_name,x_bins) in enumerate(hist_info):
                savename = '{0}_{1}_{2}'.format(process_name,cuts_name,x_name)
        
                #setup savedir
                savedir = '{0}/{1}/{2}'.format(plots_dir,process_name,x_name)
                if os.path.exists(savedir):
                        existing_files = os.listdir(savedir)
                        for fname in existing_files:
                                if cuts_name in fname:
                                        
#                                   x = input('Overwrite existing file ({0}/{1})? (y/n)'.format(savedir,fname))
                                    x = 'y'
                                    if x == 'y': #yes
                                            os.system('rm {0}'.format(os.path.join(savedir,fname)))
                                    else: #no, don't overwrite, exit
                                            print("Exiting")
                                            exiting = True
                                            break
                        if exiting: 
                                continue

                else:
                        print("Creating new directory {0}...".format(savedir))
                        try:
                            os.makedirs(savedir)
                        except OSError:
                            pass
                
                

                #main plotting function
                entry_stop = None
                plot_process_with_cuts(
                    data_paths,
                    cuts_tuple,
                    scale_variations,
                    pdf_variations,
                    x_name,
                    x_bins,
                    savename,
                    savedir,
                    entry_stop=entry_stop)

if __name__ == '__main__':
        parser = argparse.ArgumentParser()
        parser.add_argument('-c','--config_file',required=True,help='config file')
        args = parser.parse_args()
        config_file = args.config_file
        print(config_file)
        main(config_file)
        print("Finished")
