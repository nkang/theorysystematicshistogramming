To use:
./updown_plot_binning.py --config_file CONFIG_FILE.txt

This creates histograms (pdfs) with up/down scale variations of event parameters that were specified in CONFIG_FILE.txt .

Config file structure is relatively easy to understand--defines quantities to histogram, input path, output path under "plots/", cuts, and which systematic uncertainties to plot.

config_creator_binning.py is a convinience script to generate the config files more automatically

Originally authored by Derek Hamersly derek@coho.org
