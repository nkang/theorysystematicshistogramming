#!/usr/bin/env python3

import os


def parse(config_file):
        if not os.path.exists(config_file):
                raise IOError("Config file {0} not found".format(config_file))
        
        with open(config_file,'r') as f:
                lines = f.readlines()
        
        init_collectors = {'Process Name':False,
                        'Histogram Info':False,
                        'Scale Variations':False,
                        'PDF Variations':False,
                        'Cuts Tuple':False,
                        'Data Paths':False,
                        }

        #collect raw data (just lines)
        raw_data = {}
        data = {}
        for k in init_collectors: 
                raw_data[k] = []
                data[k] = []

        collectors = dict(init_collectors)

        for i,line in enumerate(lines):
                if line.startswith('#'):
                        continue
                if line.startswith('\n'):
                        collectors = dict(init_collectors)
                        continue
                if line.startswith('END CONFIG'):
                        break
                for k in collectors:
                        if line.startswith(k):
                                collectors[k] = True
                                break
                        if collectors[k]:
                                raw_data[k].append(line.strip('\n'))

        #parse raw data
        data['Process Name'] = raw_data['Process Name'][0]
        data['Scale Variations'] = {} #special case
        for line in raw_data['Histogram Info']:
                print(line)
                x_name,x_range_string = line.split('/')
                x_bins = [float(b) for b in x_range_string.strip("[]").split(",")]
                data['Histogram Info'].append((x_name,x_bins))
        for line in raw_data['Scale Variations']:
                VARname,VARstring = line.split(',')
                data['Scale Variations'][VARname] = VARstring
        for line in raw_data['PDF Variations']:
                PDFname,PDFstring = line.split(',')
                data['PDF Variations'].append((PDFname,PDFstring))
        data['Cuts Tuple'] = raw_data['Cuts Tuple'][0].split('#')
        print(data['Cuts Tuple'])
        data['Data Paths'] = raw_data['Data Paths']
        print(data['Data Paths'])

        return data['Process Name'],data['Histogram Info'],data['Scale Variations'],data['PDF Variations'],data['Cuts Tuple'],data['Data Paths']
